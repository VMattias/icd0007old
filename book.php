<?php

class Book
{
    public $title;
    public $grade;
    public $isRead;

    public function __construct($title, $grade, $isRead) {
        $this->title = $title;
        $this->grade = $grade;
        $this->isRead = $isRead;
    }

    public function __toString() : string {
        return sprintf('Title: %s, Grade: %s, isRead: %s', $this->title, $this->grade, $this->isRead);
    }

}

