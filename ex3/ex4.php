<?php

$numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];

fizzBuzz($numbers);
function fizzBuzz($list)
{
        foreach (range(1, 15) as $num) {
            if($num % 3 === 0 && $num % 5 === 0){
                print "FizzBuzz" . PHP_EOL;
            }
            elseif ($num % 3 === 0){
                print "Fizz" . PHP_EOL;
            }
            elseif ($num % 5 === 0){
                print "Buzz" . PHP_EOL;
            }
            else{
                print $num . PHP_EOL;
            }
        }
    }