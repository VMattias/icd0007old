<?php
//$temp = isset($_POST['temp']) ? $_POST['temp'] : '';
$temp = $_POST['temperature'] ?? '';

if (empty($temp)){
    $message = 'Insert temperature';
} elseif (!is_numeric($temp)) {
    $message = 'Temperature must be an integer';
} else {
    $message = $temp . " decrees in Celsius is " . ((intval($temp)) * 9/5 + 32) . " decrees in Fahrenheit";
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Celsius to Fahrenheit</title>
</head>
<body>

    <nav>
        <a href="index.html">Celsius to Fahrenheit</a> |
        <a href="f2c.html">Fahrenheit to Celsius</a>
    </nav>

    <main>

        <h3>Celsius to Fahrenheit</h3>


        <em><?= $message ?></em> /<br>

    </main>

</body>
</html>
