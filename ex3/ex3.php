<?php

$numbers = [1, 2, 5, 6, 2, 11, 2, 7];

print_r(getOddNumbers($numbers));
function getOddNumbers($list)
{
    $oddNumbers = [];
    foreach ($list as $num) {
        if($num % 2 === 1){
            $oddNumbers[] = $num;
        }
    }
    return $oddNumbers;
}
