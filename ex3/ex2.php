<?php

$numbers = [1, 2, '3', 6, 2, 3, 2, 3];

function isInList($list, $elementToBeFound) {
    foreach ($list as $number){
        if ($number === $elementToBeFound){
            return true;
        }
    }
    return false;
}