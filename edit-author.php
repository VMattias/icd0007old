<?php

//    $conn = connectDB();
//    $stmt = $conn->prepare('select * from authors');
//    $stmt->execute();
const AUTHORS_FILE = 'authors.txt';

function getAuthors()
{
    $authors = [];

    $lines = file('authors.txt');

    foreach ($lines as $line) {
        $parts = explode(';', trim($line));
        [$firstName, $lastName, $grade] = $parts;
        $author = new Author();
        $author->firstName = urldecode($firstName);
        $author->lastName = urldecode($lastName);
        $author->grade = $grade;

        array_push($authors, $author);
    }
    return $authors;
}

function getAuthorByName($firstName) {
    $authors = getAuthors();

    foreach ($authors as $author) {
        if ($author->firstName === $firstName) {
            return $author;
        }
    }
    return null;
}

function editAuthor($originalName, $firstName, $lastName, $grade){
       $authors = getAuthors();
       $data = "";

       foreach ($authors as $author) {
               if ($author["firstName"] === $originalName) {
                      $author["firstName"] = $firstName;
                      $author["lastName"] = $lastName;
                      $author["grade"] = $grade;
                  }
        $data = $data . urlencode($author["firstName"]) . ";"
               . urlencode($author["lastName"]) . ";" . $author["grade"] . PHP_EOL;
    }

    file_put_contents(AUTHORS_FILE, $data);
    header("Location: author-list.php?message=Success!");
}

function deleteAuthorByName($firstName){
        $authors = getAuthors();
        $data = "";

        foreach ($authors as $author) {
                if ($author["firstName"] !== $firstName) {
                        $data = $data . urlencode($author["firstName"]) . ";"
                                . $author["lastName"] . ";" . $author["grade"] . PHP_EOL;
                    }
        file_put_contents(AUTHORS_FILE, $data);
    }
}

$firstName = "";

if ($_SERVER["REQUEST_METHOD"] === "GET"){

    $firstName = $_GET["firstName"];
    $author = getAuthorByName($firstName);

    $firstName = $author->firstName;
    $lastName = $author->lastName;
    $grade = $author->grade;
}

if(isset($_POST["deleteButton"])){
        deleteAuthorByName($firstName);
        echo $author["firstName"];
    header("Location: author-list.php");
}

if(isset($_POST["submitButton"])){
        $originalName = $_POST["original-name"];
        if (isset($_POST['firstName'])) {
                $firstName = $_POST['firstName'];
            }
   if (isset($_POST['lastName'])) {
                $lastName = $_POST['lastName'];
            }
    if (isset($_POST['grade'])) {
                $grade = $_POST['grade'];
            }
    editAuthor($originalName, $firstName, $lastName, $grade);
    header("Location: author-list.php?message=Success!");
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="styles.css" rel="stylesheet">
    <title>Muuda või kustuta autor</title>
</head>
<body>
<nav>
    <a href="index.php" id="book-list-link">Raamatud</a>
    <span> | </span>
    <a href="book-add.php" id="book-form-link">Lisa raamat</a>
    <span> | </span>
    <a href="author-list.php" id="author-list-link">Autorid</a>
    <span> | </span>
    <a href="author-add.php" id="author-form-link">Lisa autor</a>
</nav>

<main>
    <form id="input-form" method="post" action="edit-author.php">

        <input id="original-name" name="original-name" type="hidden" value="<?=$firstName?>">

        <div class="label-cell"><label for="name">Eesnimi:</label></div>
        <div class="input-cell"><input id="name" name="firstName" type="text" value="<?=$firstName?>"></div>

        <div class="label-cell"><label for="last-name">Perekonnanimi:</label></div>
        <div class="input-cell"><input id="last-name" name="lastName" type="text" value="<?=$lastName?>"></div>

        <div class="label-cell">Hinne: </div>
        <div class="input-cell">
            <label><input <?php if ($author->grade === "1"){ echo 'checked="checked"'; }?>
                    type="radio" name="grade" value="1">1</label>
            <label><input <?php if ($author->grade === "2"){ echo 'checked="checked"'; }?>
                    type="radio" name="grade" value="2">2</label>
            <label><input <?php if ($author->grade === "3"){ echo 'checked="checked"'; }?>
                    type="radio" name="grade" value="3">3</label>
            <label><input <?php if ($author->grade === "4"){ echo 'checked="checked"'; }?>
                    type="radio" name="grade" value="4">4</label>
            <label><input <?php if ($author->grade === "5"){ echo 'checked="checked"'; }?>
                    type="radio" name="grade" value="5">5</label>
        </div>

        <div class="label-cell"></div>
        <div class="input-cell">
            <div class="buttonSubmit">
                <input name="submitButton" type="submit" value="Salvesta">
            </div>
        </div>

        <div class="label-cell"></div>
        <div class="input-cell">
            <div class="buttonDelete">
                <input name="deleteButton" type="submit" value="Kustuta">
            </div>
        </div>

    </form>
</main>

<footer>
    ICD0007: My author list
</footer>
</body>
</html>