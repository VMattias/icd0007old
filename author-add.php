<?php
include_once __DIR__ . '/author.php';

$errors=[];
$firstName="";
$lastName="";
$grade=null;


const DATA_FILE = 'C:\Users\Matti\icd0007\authors.txt';

function addNewAuthor(){
$firstName = urlencode($_POST['firstName']) ?? '';
$lastName = urlencode($_POST['lastName']) ?? '';
$grade = $_POST['grade'] ?? '';

$author = new Author($firstName, $lastName, $grade);
$whatToAdd = $author -> firstName . ";" . $author -> lastName . ";" . $author -> grade;
file_put_contents('authors.txt', $whatToAdd . PHP_EOL, FILE_APPEND);
}

if ($_SERVER["REQUEST_METHOD"] === "POST") {

    $firstName = urlencode($_POST['firstName']) ?? '';
    $lastName = urlencode($_POST['lastName']) ?? '';
    $grade = $_POST['grade'] ?? '';

    if (strlen($firstName) < 1 || strlen($firstName) > 21) {
        $errors[] = "Autori eesnimi peab olema 1 ja 21 tähemärgi vahel.";
    }
    if (strlen($lastName) < 2 || strlen($lastName) > 22) {
        $errors[] = "Autori perekonnanimi peab olema 2 ja 22 tähemärgi vahel.";
    }

    if (empty($errors)) {
        addNewAuthor();
        header("Location: author-list.php?message=Success!");
    }
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <link href="styles.css" rel="stylesheet">

        <title>Harjutustund 1</title>
    </head>
    <body id="author-form-page">

        <nav><a href="index.php" id="book-list-link">Raamatud</a>
            <span>|</span>
            <a href="book-add.php" id="book-form-link">Lisa raamat</a>
            <span>|</span>
            <a href="author-list.php" id="author-list-link">Autorid</a>
            <span>|</span>
            <a href="author-add.php" id="author-form-link">Lisa autor</a>
        </nav>

        <main>
            <div id="error-block"><?php foreach ($errors as $error) {echo "<p id='error'>$error</p>";}?></div>
            <form id="input-form" action="" method="post">

                <input name="id" type="hidden" value="">

                <div class="label-cell"><label for="name">Eesnimi:</label></div>
                <div class="input-cell"><input id="name" name="firstName" value="<?=$firstName?>" type="text"/></div>

                <div class="label-cell"><label for="last-name">Perekonnanimi:</label></div>
                <div class="input-cell"><input id="last-name" name="lastName" value="<?=$lastName?>" type="text"/></div>

                <div class="label-cell">Hinne: </div>
                <div class="input-cell">
                    <label><input <?php if ($grade === "1"){ echo 'checked="checked"'; }?>
                                type="radio" name="grade" value="1">1</label>
                    <label><input <?php if ($grade === "2"){ echo 'checked="checked"'; }?>
                                type="radio" name="grade" value="2">2</label>
                    <label><input <?php if ($grade === "3"){ echo 'checked="checked"'; }?>
                                type="radio" name="grade" value="3">3</label>
                    <label><input <?php if ($grade === "4"){ echo 'checked="checked"'; }?>
                                type="radio" name="grade" value="4">4</label>
                    <label><input <?php if ($grade === "5"){ echo 'checked="checked"'; }?>
                                type="radio" name="grade" value="5">5</label>
                </div>

                <div class="flex-break"></div>

                <div class="label-cell"></div>
                <div class="input-cell button-cell">


                    <input name="submitButton" type="submit" value="Salvesta">
                </div>

            </form>

        </main>

        <footer>
            ICD0007 Mattias Vahtra
        </footer>

    </body>
</html>