<?php
//foreach ($subjectGrades as $key => $value) {
//    print "$key;$value" . PHP_EOL;
//}

include_once __DIR__ . '/book.php';

const DATA_FILE = 'C:\Users\Matti\icd0007\books.txt';

$errors=[];
$title="";
$grade=null;
$isRead=null;

function addNewBook(){
    $title = urlencode($_POST['title']) ?? '';
    $grade = $_POST['grade'] ?? '';
    $isRead = $_POST['isRead'] ?? '';

    $book = new Book($title, $grade, $isRead);
    $whatToAdd = $book -> title . ";" . $book -> grade . ";" . $book -> isRead;
    file_put_contents('books.txt', $whatToAdd . PHP_EOL, FILE_APPEND);
}
if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $title = urlencode($_POST['title']) ?? '';
    $grade = $_POST['grade'] ?? '';
    $isRead = $_POST['isRead'] ?? '';

    if (empty($title)) {
        $errors[] = "Insert title";
    }

    if (strlen($title) < 3 || strlen($title) > 23) {
        $errors[] = "Raamatu pealkiri peab olema 3 ja 23 tähemärgi vahel.";
    }


    if (empty($errors)) {
        addNewBook();
        header("Location: index.php?message=Success!");
    }
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <link href="styles.css" rel="stylesheet">

        <title>Harjutustund 1</title>
    </head>
    <body id="book-form-page">

        <nav><a href="index.php" id="book-list-link">Raamatud</a>
            <span>|</span>
            <a href="book-add.php" id="book-form-link">Lisa raamat</a>
            <span>|</span>
            <a href="author-list.php" id="author-list-link">Autorid</a>
            <span>|</span>
            <a href="author-add.php" id="author-form-link">Lisa autor</a>
        </nav>

        <main>

            <div id="error-block"><?php foreach ($errors as $error) {echo "<p id='error'>$error</p>";}?></div>
            <form id="input-form" action="book-add.php" method="post">

                <input name="id" type="hidden" value="">

                <div class="label-cell"><label for="title">Pealkiri:</label></div>
                <div class="input-cell"><input id="title" name="title" type="text" value="<?=$title?>"></div>

<!--
                <div class="label-cell"><label for="author1">Autor 1:</label></div>
                <div class="input-cell">
                    <select id="author1" name="author1">
                        <option value=""></option>
                        <option value="1">Martin Roosileht</option><option value="2">Allan Varik</option><option value="3">Tore mees</option>
                    </select>
                </div>

                <div class="label-cell"><label for="author2">Autor 2:</label></div>
                <div class="input-cell">
                    <select id="author2" name="author2">
                        <option value=""></option>
                        <option value="1">Martin Roosileht</option><option value="2">Allan Varik</option><option value="3">Tore mees</option>
                    </select>
                </div>
-->

                <div class="label-cell">Hinne: </div>
                <div class="input-cell">
                    <label><input <?php if ($grade === "1"){ echo 'checked="checked"'; }?>
                                type="radio" name="grade" value="1">1</label>
                    <label><input <?php if ($grade === "2"){ echo 'checked="checked"'; }?>
                                type="radio" name="grade" value="2">2</label>
                    <label><input <?php if ($grade === "3"){ echo 'checked="checked"'; }?>
                                type="radio" name="grade" value="3">3</label>
                    <label><input <?php if ($grade === "4"){ echo 'checked="checked"'; }?>
                                type="radio" name="grade" value="4">4</label>
                    <label><input <?php if ($grade === "5"){ echo 'checked="checked"'; }?>
                                type="radio" name="grade" value="5">5</label>
                </div>

                <div class="flex-break"></div>

                <div class="label-cell"><label for="read">Loetud:</label></div>
                <div class="input-cell"><input id="read" name="isRead" checked="checked" type="checkbox"/></div>

                <div class="flex-break"></div>

                <div class="label-cell"></div>
                <div class="input-cell button-cell">


                    <input name="submitButton" type="submit" value="Salvesta">
                </div>

            </form>


        </main>


            <footer>
                ICD0007 Mattias Vahtra
            </footer>

    </body>
</html>