<?php
$message = "";
if ($_SERVER["REQUEST_METHOD"] === "GET") {
    $message = $_GET["message"];
}

function makeAuthorTable() {
    $lines = file('authors.txt');
    foreach ($lines as $line) {
        $parts = explode(';', trim($line));
        [$firstName, $lastName, $grade] = $parts;
        $firstName = urldecode($firstName);
        $lastName = urldecode($lastName);

        echo "<div><input type='hidden' name='original-name' value='$firstName'></div>";
        echo "<div><a href='edit-author.php?firstName=$firstName'>$firstName</a></div>";

        echo "<div><p>$lastName</p></div>";

        echo "<div class='score-empty'>";
            for ($i = 0; $i < $grade; $i++){
            echo "<span class='score-filled'>★</span>";
            }
            for ($i = 0; $i < 5 - (int)$grade; $i++){
            echo "<span class='score-empty'>★</span>";
            }

            echo "</div>";
        echo "<div class='flex-break'></div>";
}
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <link href="styles.css" rel="stylesheet">

        <title>Harjutustund 1</title>
    </head>
    <body id="author-list-page">

        <nav><a href="index.php" id="book-list-link">Raamatud</a>
            <span>|</span>
            <a href="book-add.php" id="book-form-link">Lisa raamat</a>
            <span>|</span>
            <a href="author-list.php" id="author-list-link">Autorid</a>
            <span>|</span>
            <a href="author-add.php" id="author-form-link">Lisa autor</a>
        </nav>

        <main>

            <h1 id="message-block"><?=$message?></h1>
            <div id="author-list">

                <div class="title-cell header-cell">Eesnimi</div>
                <div class="author-cell header-cell">Perekonnanimi</div>
                <div class="grade-cell header-cell">Hinne</div>

                <div class="flex-break header-divider"></div>

                <?php makeAuthorTable(); ?>

                <div>
                    <p>Tore</p>
                </div>


                <div>
                    <p>inimene</p>
                </div>

                <div class="score-empty">
                    <span class="score-filled">&#9733;</span><span class="score-filled">&#9733;</span><span class="score-filled">&#9733;</span><span class="score-filled">&#9733;</span><span class="score-filled">&#9733;</span>
                </div>

                <div class="flex-break"></div>

                <div>

                    <p>Tore</p>

                </div>

                <div>
                    <p>naine</p>
                </div>

                <div class="score-empty">
                    <span class="score-filled">&#9733;</span><span class="score-filled">&#9733;</span><span class="score-filled">&#9733;</span><span class="score-filled">&#9733;</span><span>&#9733;</span>
                </div>

                <div class="flex-break"></div>

                <div>
                    <p>Tore</p>
                </div>

                <div>
                    <p>mees</p>
                </div>

                <div class="score-empty">
                    <span class="score-filled">&#9733;</span><span class="score-filled">&#9733;</span><span class="score-filled">&#9733;</span><span class="score-filled">&#9733;</span><span>&#9733;</span>
                </div>

                <div class="flex-break"></div>

            </div>

        </main>

            <footer>
                ICD0007 Mattias Vahtra
            </footer>

    </body>
</html>