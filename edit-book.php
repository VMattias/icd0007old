<?php

require_once("functions.php");

$title = $_GET["title"];
$authorId = null;
$grade = null;
$isRead = null;


if ($_SERVER["REQUEST_METHOD"] === "GET"){

    $title = $_GET["title"];

    $book = getBookByTitle($title);
    $authorName = $book->authorName;
    $id = $book->id;
    $authorId = $book->authorId;
    $grade = $book->grade;
    $isRead = $book->isRead;
}

if(isset($_POST["deleteButton"])){
    $title = $_POST["title"];
    $book = getBookByTitle($title);
    $id = $book->id;
    deleteBook($id);
}

if(isset($_POST["submitButton"])){
    $originalTitle = $_POST["original-title"];
    $book = getBookByTitle($originalTitle);
    $bookId = $book->id;

    $title = isset($_POST["title"]) ? $_POST["title"] : "";
    $grade = isset($_POST["grade"]) ? $_POST["grade"] : null;
    $authorId = isset($_POST["author1"]) ? $_POST["author1"] : null;
    $isRead = isset($_POST["isRead"]) ? $_POST["isRead"] : null;

    editBook($bookId, $title, $authorId, $grade, $isRead);
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link href="styles.css" rel="stylesheet">
        <title>Muuda või kustuta raamat</title>
    </head>
    <body>
        <nav>
            <a href="index.php" id="book-list-link">Raamatud</a>
            <span> | </span>
            <a href="book-add.php" id="book-form-link">Lisa raamat</a>
            <span> | </span>
            <a href="author-list.php" id="author-list-link">Autorid</a>
            <span> | </span>
            <a href="author-add.php" id="author-form-link">Lisa autor</a>
        </nav>

        <main>
            <form id="input-form" method="post" action="edit-book.php">

                <input id="original-title" name="original-title" type="hidden" value="<?=$title?>">

                <div class="label-cell"><label for="title">Pealkiri:</label></div>
                <div class="input-cell"><input id="title" name="title" type="text" value="<?=$title?>"></div>

                <div class="label-cell"><label for="author1">Autor:</label></div>
                <div class="input-cell">
                    <select id="author1" name="author1">
                        <option value=<?=$authorId?>><?=$authorName?></option>
                        <?php makeAuthorsForAddBook()?>
                    </select>
                </div>


                <div class="label-cell">Hinne: </div>
                <div class="input-cell">
                    <label><input <?php if ($book->grade === "1"){ echo 'checked="checked"'; }?>
                                type="radio" name="grade" value="1">1</label>
                    <label><input <?php if ($book->grade === "2"){ echo 'checked="checked"'; }?>
                                type="radio" name="grade" value="2">2</label>
                    <label><input <?php if ($book->grade === "3"){ echo 'checked="checked"'; }?>
                                type="radio" name="grade" value="3">3</label>
                    <label><input <?php if ($book->grade === "4"){ echo 'checked="checked"'; }?>
                                type="radio" name="grade" value="4">4</label>
                    <label><input <?php if ($book->grade === "5"){ echo 'checked="checked"'; }?>
                                type="radio" name="grade" value="5">5</label>
                </div>

                <div class="flex-break"></div>

                <div class="label-cell"><label for="read">Loetud:</label></div>
                <div class="input-cell"><input id="read" name="isRead" value="1"
                        <?php if ($isRead == 1) {echo 'checked="checked"' ;} ?> type="checkbox"></div>

                <div class="flex-break"></div>

                <div class="label-cell"></div>
                <div class="input-cell">
                    <div class="buttonSubmit">
                        <input name="submitButton" type="submit" value="Salvesta">
                    </div>
                </div>

                <div class="label-cell"></div>
                <div class="input-cell">
                    <div class="buttonDelete">
                        <input name="deleteButton" type="submit" value="Kustuta">
                    </div>
                </div>

            </form>
        </main>

        <footer>
ICD0007: My book list
        </footer>
    </body>
</html>