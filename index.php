<?php

$message = "";

if ($_SERVER["REQUEST_METHOD"] === "GET") {
    $message = $_GET["message"];
}

function getBooks(){
    $lines = file('books.txt');
    $books = [];
    print_r($lines);
    foreach ($lines as $line) {
        $parts = explode(';', trim($line));
        //$title = $parts[0];
        //$author = $parts[1];
        //$grade = $parts[2];
        //made shorter with the next line
        [$title, $grade, $isRead] = $parts;
        $book = new Book($title, $grade, $isRead);
        print_r("wtf". $book);
        array_push($books, $book);
    }
    return $books;
}
function makeBookTable() {
    $lines = file('books.txt');
    foreach ($lines as $line) {
        $parts = explode(';', trim($line));
        [$title, $grade, $isRead] = $parts;
        $title = urldecode($title);

        echo "<div><p>$title</p></div>";

        echo "<div><p>autori nimi</p></div>";

        echo "<div class='score-empty'>";
        for ($i = 0; $i < $grade; $i++){
            echo "<span class='score-filled'>★</span>";
        }
        for ($i = 0; $i < 5 - (int)$grade; $i++){
            echo "<span class='score-empty'>★</span>";
        }

        echo "</div>";
        echo "<div class='flex-break'></div>";
    }
}

?>

<!DOCTYPE html>
<html lang="et">
    <head>
        <meta charset="utf-8">
        <link href="styles.css" rel="stylesheet">

        <title>Harjutustund 1</title>
    </head>
    <body id="book-list-page">

        <nav>
            <a href="index.php" id="book-list-link">Raamatud</a>
            <span>|</span>
            <a href="book-add.php" id="book-form-link">Lisa raamat</a>
            <span>|</span>
            <a href="author-list.php" id="author-list-link">Autorid</a>
            <span>|</span>
            <a href="author-add.php" id="author-form-link">Lisa autor</a>
        </nav>


        <main>
            <h1 id="message-block"><?=$message?></h1>
            <div id="book-list">

                <div class="title-cell header-cell">Pealkiri</div>
                <div class="author-cell header-cell">Autorid</div>
                <div class="grade-cell header-cell">Hinne</div>

                <div class="flex-break header-divider"></div>

                <?php makeBookTable(); ?>
                <!--       echo "<input type='hidden' name='original-title' value='$title'>";-->
                <div>



                    <p>Vaimne Tervis ja Kuidas Seda Hoida</p>

                </div>

                <div>

                </div>

                <div class="score-empty">
                    <span class="score-filled">&#9733;</span><span class="score-filled">&#9733;</span><span class="score-filled">&#9733;</span><span class="score-filled">&#9733;</span><span class="score-filled">&#9733;</span>
                </div>

                <div class="flex-break"></div>

                <div>



                    <p>Elu on raske, aga ei pea olema</p>

                </div>

                <div>

                </div>

                <div class="score-empty">
                    <span class="score-filled">&#9733;</span><span class="score-filled">&#9733;</span><span class="score-filled">&#9733;</span><span class="score-filled">&#9733;</span><span>&#9733;</span>
                </div>

                <div class="flex-break"></div>

                <div>



                    <p>Tore oleks niisama puhata</p>

                </div>

                <div>

                </div>

                <div class="score-empty">
                    <span class="score-filled">&#9733;</span><span class="score-filled">&#9733;</span><span class="score-filled">&#9733;</span><span class="score-filled">&#9733;</span><span>&#9733;</span>
                </div>

                <div class="flex-break"></div>

            </div>


        </main>

            <footer>
                ICD0007 Mattias Vahtra
            </footer>
    </body>
</html>