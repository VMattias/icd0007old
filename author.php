<?php


class Author
{
    public $firstName;
    public $lastName;
    public $grade;

    public function __construct($firstName, $lastName, $grade){

        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->grade = $grade;
    }

}