<?php
require_once("functions.php");

$errors=[];
$title="";
$grade=null;
$authorId=null;
$isRead=null;

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $title = isset($_POST["title"]) ? $_POST["title"] : "";
    $grade = isset($_POST["grade"]) ? $_POST["grade"] : null;
    $authorId = isset($_POST["author1"]) ? $_POST["author1"] : null;
    $isRead = isset($_POST["isRead"]) ? $_POST["isRead"] : null;

    if (empty($title)) {
        $errors[] = "Insert title";
    }

    if (strlen($title) < 3 || strlen($title) > 23) {
        $errors[] = "Raamatu pealkiri peab olema 3 ja 23 tähemärgi vahel.";
    }

    if ($authorId === "") {
        $authorId = null;
    }

    if (empty($errors)) {
        addBook($title, $grade, $authorId, $isRead);
    }

}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link href="styles.css" rel="stylesheet">
        <title>Lisa raamat</title>
    </head>
    <body>
        <nav>
            <a href="index.php" id="book-list-link">Raamatud</a>
            <span> | </span>
            <a href="book-add.php" id="book-form-link">Lisa raamat</a>
            <span> | </span>
            <a href="author-list.php" id="author-list-link">Autorid</a>
            <span> | </span>
            <a href="author-add.php" id="author-form-link">Lisa autor</a>
        </nav>

        <main>
            <div id="error-block"><?php foreach ($errors as $error) {echo "<p id='error'>$error</p>";}?></div>
            <form id="input-form" method="post" action="book-add.php">

                <input name="id" type="hidden" value="">

                <div class="label-cell"><label for="title">Pealkiri:</label></div>
                <div class="input-cell"><input id="title" name="title" type="text" value="<?=$title?>"></div>


                <div class="label-cell"><label for="author1">Autor:</label></div>
                <div class="input-cell">
                    <select id="author1" name="author1">
                        <option value=""></option>
                        <?php makeAuthorsForAddBook()?>
                    </select>
                </div>


                <div class="label-cell">Hinne: </div>
                <div class="input-cell">
                    <label><input <?php if ($grade === "1"){ echo 'checked="checked"'; }?>
                                type="radio" name="grade" value="1">1</label>
                    <label><input <?php if ($grade === "2"){ echo 'checked="checked"'; }?>
                                type="radio" name="grade" value="2">2</label>
                    <label><input <?php if ($grade === "3"){ echo 'checked="checked"'; }?>
                                type="radio" name="grade" value="3">3</label>
                    <label><input <?php if ($grade === "4"){ echo 'checked="checked"'; }?>
                                type="radio" name="grade" value="4">4</label>
                    <label><input <?php if ($grade === "5"){ echo 'checked="checked"'; }?>
                                type="radio" name="grade" value="5">5</label>
                </div>

                <div class="flex-break"></div>

                <div class="label-cell"><label for="read">Loetud:</label></div>
                <div class="input-cell"><input id="read" name="isRead" value="1" type="checkbox"></div>

                <div class="flex-break"></div>

                <div class="label-cell"></div>
                <div class="input-cell">
                    <div class="buttonSubmit">
                        <input name="submitButton" type="submit" value="Salvesta">
                    </div>
                </div>

            </form>
        </main>

        <footer>
            ICD0007: My book list
        </footer>
    </body>
</html>