<?php
require_once ("functions.php");

$message = "";
if ($_SERVER["REQUEST_METHOD"] === "GET") {
    $message = $_GET["message"];
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link href="styles.css" rel="stylesheet">
        <title>Raamatute nimekiri</title>
    </head>
    <body>
        <nav>
            <a href="index.php" id="book-list-link">Raamatud</a>
            <span> | </span>
            <a href="book-add.php" id="book-form-link">Lisa raamat</a>
            <span> | </span>
            <a href="author-list.php" id="author-list-link">Autorid</a>
            <span> | </span>
            <a href="author-add.php" id="author-form-link">Lisa autor</a>
        </nav>

        <main>

            <h1 id="message-block"><?=$message?></h1>
            <div id="book-list">
                <div class="title-cell header-cell">Pealkiri</div>
                <div class="author-cell header-cell">Autorid</div>
                <div class="grade-cell header-cell">Hinne</div>

                <hr class="header-divider">


                <?php makeBookTable();?>

            </div>
        </main>

        <footer>
            ICD0007: My book list
        </footer>
    </body>
</html>