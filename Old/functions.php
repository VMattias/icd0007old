<?php
require_once("book.php");
require_once("author.php");



function connectDB() {
    $host = 'db.mkalmo.xyz';
    $username = 'vmattias';
    $pass = '210C';
    try {
        $address = sprintf('mysql:host=%s;dbname=%s', $host, $username);
        return new PDO ($address, $username, $pass,
            [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
    } catch(PDOException $e) {
        throw new RuntimeException("Can't connect!");
    }
}


function _OLD_getBooks() {
    $conn = connectDB();
    $stmt = $conn->prepare('select * from books left join authors on books.author_id = authors.author_id');
    $stmt->execute();
    $books = [];
    foreach ($stmt as $row){
        $book = new Book(
            $row["book_id"],
            $row["title"],
            $row["grade"],
            $row["author_id"],
            $row["author_name"],
            $row["is_read"]);
        array_push($books, $book);

    }
    return $books;
}

function _OLD_makeBookTable() {
    $books = getBooks();

    foreach ($books as $book) {
        $title = $book->title;
        $author = $book->authorName;
        $grade = $book->grade;

        echo "<form method='get'>";
        echo "<input type='hidden' name='original-title' value='$title'>";
        echo "<a href='edit-book.php?title=$title'><div>$title</div></a>";
        echo "</form>";

        echo "<div>$author</div>";

        echo "<div class='score-empty'>";
        for ($i = 0; $i < $grade; $i++){
            echo "<span class='score-filled'>★</span>";
        }
        for ($i = 0; $i < 5 - (int)$grade; $i++){
            echo "<span class='score-empty'>★</span>";
        }

        echo "</div>";
        echo "<div class='flex-break'></div>";
    }
}

function getBookByTitle($title) {
    $books = getBooks();

    foreach ($books as $book) {
        if ($book->title === $title) {
            return $book;
        }
    }
    return null;
}

function addBook($title, $grade, $authorId, $isRead) {
    $conn = connectDB();
    $authorName = null;
    $stm = $conn->prepare('SELECT author_id, first_name, last_name FROM authors');
    $stm->execute();

    foreach ($stm as $author){
        if ($authorId === $author["author_id"]){
            $authorName = $author["first_name"] . " " . $author["last_name"];
        }
    }

    $stmt = $conn->prepare('INSERT INTO books (title, grade, author_id, is_read, author_name)
                                VALUES(:title, :grade, :author_id, :is_read, :author_name)');
    $stmt->bindValue(':title', $title);
    $stmt->bindValue(':grade', $grade);
    $stmt->bindValue(':author_id', $authorId);
    $stmt->bindValue(':is_read', $isRead);
    $stmt->bindValue(':author_name', $authorName);
    $stmt->execute();
    header("Location: index.php?message=Success!");
}


function editBook($bookId, $title, $authorId, $grade, $isRead){
    $conn = connectDB();
    $authorName = null;
    $stm = $conn->prepare('SELECT author_id, first_name, last_name FROM authors');
    $stm->execute();

    foreach ($stm as $author){
        if ($authorId === $author["author_id"]){
            $authorName = $author["first_name"] . " " . $author["last_name"];
        }
    }
    if ($authorId == 0 || $authorId == "") {
        $authorId = null;
    }
    $stmt = $conn->prepare("UPDATE books SET title = :title, grade = :grade, author_id = :author_id,
                                    is_read = :is_read, author_name = :author_name WHERE books.book_id = :book_id");
    $stmt->bindValue(':book_id', $bookId);
    $stmt->bindValue(':title', $title);
    $stmt->bindValue(':grade', $grade);
    $stmt->bindValue(':author_id', $authorId);
    $stmt->bindValue(':is_read', $isRead);
    $stmt->bindValue(':author_name', $authorName);

    $stmt->execute();
    header("Location: index.php?message=Success!");
}

function deleteBook($id){
    $conn = connectDB();
    print_r($id);
    $sql = $conn->prepare("DELETE FROM books WHERE book_id=" . $id);
    $sql->execute();
    header("Location: index.php?message=Success!");
}



//--------------------------AUTHOR FUNCTIONS-----------------------------


function getAuthors() {
    $conn = connectDB();
    $stmt = $conn->prepare('select * from authors');
    $stmt->execute();
    $authors = [];
    foreach ($stmt as $row){
        $author = new Author(
            $row["author_id"],
            $row["first_name"],
            $row["last_name"],
            $row["author_grade"]);
        array_push($authors, $author);
    }
    return $authors;
}

function makeAuthorsForAddBook()
{
    $authors = getAuthors();
    foreach ($authors as $author) {
        $firstName = $author->firstName;
        $lastName = $author->lastName;
        $authorId = $author->authorId;
        $fullName = $firstName . " " . $lastName;
        echo "<option value=$authorId>$fullName</option>";
    }
}


function getAuthorByName($firstName) {
    $authors = getAuthors();

    foreach ($authors as $author) {
        if ($author->firstName === $firstName) {
            return $author;
        }
    }
    return null;
}


function addAuthor($firstName, $lastName, $grade)
{
    $conn = connectDB();

    $stmt = $conn->prepare('INSERT INTO authors (first_name, last_name, author_grade)
                                VALUES(:first_name, :last_name, :author_grade)');
    $stmt->bindValue(':first_name', $firstName);
    $stmt->bindValue(':last_name', $lastName);
    $stmt->bindValue(':author_grade', $grade);
    $stmt->execute();
}


function editAuthor($id, $firstName, $lastName, $grade){
    $conn = connectDB();
   // print_r($firstName);
   // print_r($lastName);
   // print_r($grade);
   // print_r($id);

    $stmt = $conn->prepare("UPDATE authors SET first_name = :first_name, last_name = :last_name, author_grade = :author_grade 
                                        WHERE authors.author_id = :author_id");
    $stmt->bindValue(':first_name', $firstName);
    $stmt->bindValue(':last_name', $lastName);
    $stmt->bindValue(':author_grade', $grade);
    $stmt->bindValue(':author_id', $id);
    $stmt->execute();

    header("Location: author-list.php?message=Success!");
}


function deleteAuthorById($id){
    $conn = connectDB();
    $sql = $conn->prepare("DELETE FROM authors WHERE author_id=" . $id);
    $sql->execute();
    header("Location: author-list.php");
}

function makeAuthorTable() {
    $authors = getAuthors();

    foreach ($authors as $author) {
        $firstName = $author->firstName;
        $lastName = $author->lastName;
        $grade = $author->grade;

        echo "<form method='get'>";
        echo "<input type='hidden' name='original-name' value='$firstName'>";
        echo "<a href='edit-author.php?firstName=$firstName'><div>$firstName</div></a>";
        echo "<div>$lastName</div>";
        echo "</form>";

        echo "<div class='score-empty'>";
        for ($i = 0; $i < $grade; $i++){
            echo "<span class='score-filled'>★</span>";
        }
        for ($i = 0; $i < 5 - (int)$grade; $i++){
            echo "<span class='score-empty'>★</span>";
        }

        echo "</div>";
        echo "<div class='flex-break'></div>";
    }
}
