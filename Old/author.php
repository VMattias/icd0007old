<?php


class Author
{
    public $authorId;
    public $firstName;
    public $lastName;
    public $grade;

    public function __construct($authorId, $firstName, $lastName, $grade){

        $this->authorId = $authorId;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->grade = $grade;
    }

}