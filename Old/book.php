
class Book
{
    public $id;
    public $title;
    public $grade;
    public $authorId;
    public $authorName;
    public $isRead;

    public function __construct($id, $title, $grade, $authorId, $authorName, $isRead)
    {
        $this->id = $id;
        $this->title = $title;
        $this->grade = $grade;
        $this->authorId = $authorId;
        $this->authorName = $authorName;
        $this->isRead = $isRead;
    }

}