<?php

const USERNAME = 'vmattias';
const PASSWORD = '951237';

function getConnection() : PDO {
    $host = 'db.mkalmo.xyz';

    $address = sprintf('mysql:host=%s;port=3306;dbname=%s',
        $host, USERNAME);

    return new PDO($address, USERNAME, PASSWORD,
        [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
}
